# Serde Json Map to todo.txt

Get [Task](https://crates.io/crates/todo-txt) struct from [Serde Json Map](https://docs.rs/serde_json/latest/serde_json/struct.Map.html).

## Features:

- `builder` - add `FieldMappingBuilder` struct.