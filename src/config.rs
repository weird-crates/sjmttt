use serde::{Deserialize, Serialize};

#[derive(Serialize,PartialEq, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct TaskListMappingConfig {
    /// Field name for ID
    #[serde(alias = "id-property-name", alias = "idPropertyName")]
    pub id_property_name: String,

    #[serde(alias = "target-field-name", alias = "targetFieldName")]
    pub target_field_name: String,

    #[serde(alias = "field-mappings", alias = "fieldMappings")]
    pub field_mappings: Vec<FieldMapping>
}

#[derive(Serialize, Deserialize, PartialEq, Debug, Clone)]
#[serde(rename_all = "camelCase")]
pub struct FieldMapping {
    /// Original field name
    #[serde(alias = "original-field-name", alias = "originalFieldName")]
    pub original_field_name: String,

    /// Target field name
    #[serde(alias = "target-field-name", alias = "targetFieldName")]
    pub target_field_name: String,

    /// Field type
    #[serde(alias = "field-type", alias = "fieldType")]
    pub field_type: TaskFieldType,

    /// Value for open task. For `FieldType::CompletionStatus` only.
    #[serde(alias = "open-value", alias = "openValue")]
    pub open_value: String,

    /// Value for completed task. For `FieldType::CompletionStatus` only.
    #[serde(alias = "completed-value", alias = "completedValue")]
    pub completed_value: String,

    /// Target datetime format. For `FieldType::Date` only.
    #[serde(alias = "target-datetime-format", alias = "targetDatetimeFormat")]
    pub target_datetime_format: String
}

#[derive(Serialize, Deserialize, PartialEq, Debug, Clone)]
pub enum TaskFieldType {
    /// Meaningless text
    Text,

    /// Completion status
    CompletionStatus,

    /// Date time in popular format (RFC 3339, etc.)
    Date,

    /// Skip processing
    Skip
}