use crate::{FieldMapping, TaskFieldType};

pub struct FieldMappingBuilder {
    original_field_name: String,
    target_field_name: String,
    field_type: TaskFieldType,
    open_value: String,
    completed_value: String,
    target_datetime_format: String
}

impl FieldMappingBuilder {
    pub fn new() -> FieldMappingBuilder {
        FieldMappingBuilder {
            original_field_name: "".to_string(),
            target_field_name: "".to_string(),
            field_type: TaskFieldType::Text,
            open_value: "".to_string(),
            completed_value: "".to_string(),
            target_datetime_format: "".to_string()
        }
    }

    pub fn set_original_field_name(&mut self, original_field_name: &str) -> &mut Self {
        self.original_field_name = original_field_name.to_string();
        self
    }

    pub fn set_target_field_name(&mut self, target_field_name: &str) -> &mut Self {
        self.target_field_name = target_field_name.to_string();
        self
    }

    pub fn set_open_value(&mut self, value: &str) -> &mut Self {
        self.open_value = value.to_string();
        self
    }

    pub fn set_completed_value(&mut self, value: &str) -> &mut Self {
        self.completed_value = value.to_string();
        self
    }

    pub fn set_field_type(&mut self, field_type: TaskFieldType) -> &mut Self {
        self.field_type = field_type;
        self
    }

    pub fn set_target_datetime_format(&mut self, target_datetime_format: &str) -> &mut Self {
        self.target_datetime_format = target_datetime_format.to_string();
        self
    }

    pub fn build(&self) -> FieldMapping {
        FieldMapping {
            original_field_name: self.original_field_name.to_string(),
            target_field_name: self.target_field_name.to_string(),
            field_type: self.field_type.clone(),
            open_value: self.open_value.to_string(),
            completed_value: self.completed_value.to_string(),
            target_datetime_format: self.target_datetime_format.to_string()
        }
    }
}