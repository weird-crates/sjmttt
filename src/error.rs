use thiserror::Error;

#[derive(Error, Debug)]
pub enum ParseError {

    #[error("todo.txt format parse error")]
    TodoTxtParseError(#[from] todo_txt::Error),

    #[error("Parse error")]
    Error
}