use fake::{Fake, Faker};
use log::LevelFilter;
use serde_json::{Map, Value};
use todo_txt::Task;

pub fn init_stdout_logging() {
    let _ = env_logger::builder().filter_level(LevelFilter::Debug)
        .is_test(true).try_init();
}

pub fn get_task_map_with_tags(test_data: &TestData) -> Map<String, Value> {
    let mut task: Map<String, Value> = Map::new();
    task.insert(test_data.subject_field_name.clone(), Value::from(test_data.subject.clone()));
    task.insert(test_data.tag1.to_string(), Value::from(test_data.tag1_value.clone()));
    task.insert(test_data.tag2.to_string(), Value::from(test_data.tag2_value.clone()));
    task
}

pub struct TestData {
    pub context1: String,
    pub context2: String,
    pub project1: String,
    pub project2: String,
    pub subject_field_name: String,
    pub subject: String,
    pub tag1: String,
    pub target_tag1: String,
    pub tag1_value: String,
    pub tag2: String,
    pub target_tag2: String,
    pub tag2_value: String
}

impl TestData {
    pub fn new() -> TestData {
        let context1 = Faker.fake::<String>();
        let context2 = Faker.fake::<String>();

        let project1 = Faker.fake::<String>();
        let project2 = Faker.fake::<String>();

        let tag1 = Faker.fake::<String>();
        let target_tag1 = Faker.fake::<String>();
        let tag1_value = Faker.fake::<String>();
        let tag2 = Faker.fake::<String>();
        let target_tag2 = Faker.fake::<String>();
        let tag2_value = Faker.fake::<String>();

        let subject: String = format!("Update version to {}:{} and {}:{} @{} +{} +{} @{}",
                      tag1.clone(), tag1_value.clone(), tag2.clone(), tag2_value.clone(),
            context1.clone(), project1.clone(), project2.clone(), context2.clone()
        );

        TestData {
            context1, context2, project1, project2,
            subject_field_name: Faker.fake::<String>(),
            subject,
            tag1,
            target_tag1,
            tag1_value,
            tag2,
            target_tag2,
            tag2_value
        }
    }
}

// ============================================
//  Asserts
// ============================================

pub fn assert_tags<'a>(task: &'a Task, tag1_name: &'a str, tag1_value: &'a str,
                   tag2_name: &'a str, tag2_value: &'a str) {
    assert_eq!(2, task.tags.len());
    let tags = task.tags.clone();
    assert_eq!(&tag1_value, &tags.get(&tag1_name.to_string()).unwrap());
    assert_eq!(&tag2_value, &tags.get(&tag2_name.to_string()).unwrap());
}