use std::str::FromStr;

use chrono::{DateTime, Utc};
use log::{debug, error, info, warn};
use serde_json::{Map, Value};
use todo_txt::Task;

use crate::config::{FieldMapping, TaskFieldType};
use crate::error::ParseError;

pub mod config;
pub mod error;

#[cfg(feature = "builder")]
pub mod builder;

#[cfg(test)]
pub mod test_utils;

/// Get [Task](https://crates.io/crates/todo-txt) from
/// [Serde Json Map](https://docs.rs/serde_json/latest/serde_json/struct.Map.html)
pub fn get_todotxt_task_from_map(task_list: &Map<String, Value>,
                                 tasklist_field_mappings: &Vec<FieldMapping>,
                                 task: &Map<String, Value>,
                                 task_field_mappings: &Vec<FieldMapping>,
                                 subject_field_name: &str,
) -> Result<Task, ParseError> {
    info!("get todo.txt task string from map");
    debug!("task list {:?}", task_list);
    debug!("task list field mappings {:?}", tasklist_field_mappings);
    debug!("task {:?}", task);
    debug!("task field mappings {:?}", task_field_mappings);

    match task.get(subject_field_name) {
        Some(subject) => {
            let mut subject_str = subject.as_str().unwrap().to_string();

            let mut row_builder: Vec<String> = Vec::new();

            let mut finished = false;

            let mut has_error = false;

            for (field, value) in task_list {
                let mapping_found = tasklist_field_mappings.iter()
                    .find(|field_mapping| {
                        debug!("original field name '{}'", field_mapping.original_field_name);
                        return &field_mapping.original_field_name == field;
                    });

                match mapping_found {
                    Some(field_mapping) => {
                        debug!("field mapping: {:?}", field_mapping);

                        let value_str = value.as_str().unwrap();
                        debug!("field value: '{}'", value_str);

                        match field_mapping.field_type {
                            TaskFieldType::Text => {
                                if field_mapping.original_field_name.len() > 0 {
                                    let updated_tag = format!("{}:{}", &field_mapping.target_field_name, &value_str);
                                    row_builder.push(updated_tag);

                                    let original_tag = format!("{}:{}", &field_mapping.original_field_name, &value_str);

                                    subject_str = subject_str.replace(&original_tag, "");
                                } else {
                                    warn!("field mapping original field name is blank, skip");
                                }
                            }
                            TaskFieldType::Date => {
                                match value_str.parse::<DateTime<Utc>>() {
                                    Ok(parsed_date) => {
                                        let target_value = parsed_date
                                            .format(&field_mapping.target_datetime_format)
                                            .to_string();

                                        let updated_tag = format!(
                                            "{}:{}", &field_mapping.target_field_name,
                                            &target_value
                                        );
                                        row_builder.push(updated_tag);

                                        let original_tag = format!("{}:{}", &field_mapping.original_field_name, &value_str);
                                        subject_str = subject_str.replace(&original_tag, "");
                                    }
                                    Err(e) => {
                                        error!("couldn't parse datetime from '{}': {}", value_str, e);
                                        has_error = true;
                                        break;
                                    }
                                }
                            }
                            TaskFieldType::CompletionStatus => {
                                if field_mapping.open_value != value_str {
                                    finished = true;
                                    debug!("completion status has been updated to true");
                                }
                            }
                            _ => {
                                let original_tag = format!("{}:{}",
                                                           &field_mapping.original_field_name, &value_str);
                                subject_str = subject_str.replace(&original_tag, "");
                            }
                        }
                    }
                    None => debug!("field mapping config wasn't found for field '{}', skip", field)
                }
            }

            for (field, value) in task {
                if has_error {
                    break;
                }

                debug!("field: '{}'", field);
                let mapping_found = task_field_mappings.iter()
                    .find(|field_mapping| {
                        debug!("original field name '{}'", field_mapping.original_field_name);
                        return &field_mapping.original_field_name == field;
                    });

                match mapping_found {
                    Some(field_mapping) => {
                        debug!("field mapping: {:?}", field_mapping);

                        let value_str = value.as_str().unwrap();
                        debug!("field value: '{}'", value_str);

                        match field_mapping.field_type {
                            TaskFieldType::Text => {
                                if field_mapping.original_field_name.len() > 0 {
                                    let updated_tag = format!("{}:{}", &field_mapping.target_field_name, &value_str);
                                    row_builder.push(updated_tag);

                                    let original_tag = format!("{}:{}", &field_mapping.original_field_name, &value_str);

                                    subject_str = subject_str.replace(&original_tag, "");
                                } else {
                                    warn!("field mapping original field name is blank, skip");
                                }
                            }
                            TaskFieldType::Date => {
                                match value_str.parse::<DateTime<Utc>>() {
                                    Ok(parsed_date) => {
                                        let target_value = parsed_date
                                            .format(&field_mapping.target_datetime_format)
                                            .to_string();

                                        let updated_tag = format!(
                                            "{}:{}", &field_mapping.target_field_name,
                                            &target_value
                                        );
                                        row_builder.push(updated_tag);

                                        let original_tag = format!("{}:{}", &field_mapping.original_field_name, &value_str);
                                        subject_str = subject_str.replace(&original_tag, "");
                                    }
                                    Err(e) => {
                                        error!("couldn't parse datetime from '{}': {}", value_str, e);
                                        has_error = true;
                                        break;
                                    }
                                }
                            }
                            TaskFieldType::CompletionStatus => {
                                if field_mapping.open_value != value_str {
                                    finished = true;
                                    debug!("completion status has been updated to true");
                                }
                            }
                            _ => {
                                let original_tag = format!("{}:{}",
                                                           &field_mapping.original_field_name, &value_str);
                                subject_str = subject_str.replace(&original_tag, "");
                            }
                        }
                    }
                    None => debug!("field mapping config wasn't found for field '{}', skip", field)
                }
            }

            if !has_error {
                row_builder.push(subject_str.to_string());

                let task_row = row_builder.join(" ");
                let mut result = Task::from_str(&task_row)?;
                result.finished = finished;

                debug!("task: {}", result);

                Ok(result)
            } else {
                Err(ParseError::Error)
            }
        }
        None => {
            error!("expected subject field '{}' is missing in task map", subject_field_name);
            Err(ParseError::Error)
        }
    }
}

#[cfg(test)]
mod tasklist_mapping_tests {
    use serde_json::{Map, Value};
    use crate::builder::FieldMappingBuilder;
    use crate::{FieldMapping, get_todotxt_task_from_map, TaskFieldType};
    use crate::test_utils::{assert_tags, get_task_map_with_tags, init_stdout_logging, TestData};

    #[test]
    fn tasklist_field_mapping_test() {
        init_stdout_logging();

        let test_data = TestData::new();

        let task_list: Map<String, Value> = get_task_map_with_tags(&test_data);

        let tag1_mapping = FieldMappingBuilder::new()
            .set_original_field_name(&test_data.tag1)
            .set_target_field_name(&test_data.target_tag1)
            .set_field_type(TaskFieldType::Text)
            .build();

        let tag2_mapping = FieldMappingBuilder::new()
            .set_original_field_name(&test_data.tag2)
            .set_target_field_name(&test_data.target_tag2)
            .set_field_type(TaskFieldType::Text)
            .build();

        let tasklist_field_mappings: Vec<FieldMapping> = vec![tag1_mapping, tag2_mapping];

        let task = get_task_map_with_tags(&test_data);
        
        let task_field_mappings: Vec<FieldMapping> = vec![];

        let task = get_todotxt_task_from_map(
            &task_list, &tasklist_field_mappings,
            &task,
            &task_field_mappings,
            &test_data.subject_field_name,
        ).unwrap();

        assert_tags(&task, &test_data.target_tag1, &test_data.tag1_value,
                    &test_data.target_tag2, &test_data.tag2_value);
    }
}

#[cfg(test)]
mod task_mapping_tests {
    use fake::{Fake, Faker};
    use serde_json::{Map, Value};

    use crate::{FieldMapping, get_todotxt_task_from_map};
    use crate::builder::FieldMappingBuilder;
    use crate::config::TaskFieldType;
    use crate::test_utils::{assert_tags, get_task_map_with_tags, init_stdout_logging, TestData};

    #[test]
    fn completed_status_should_be_mapped() {
        init_stdout_logging();

        let subject_field_name: String = Faker.fake::<String>();
        let subject: String = Faker.fake::<String>();

        let task_list: Map<String, Value> = Map::new();
        let tasklist_field_mappings: Vec<FieldMapping> = Vec::new();
        let mut task: Map<String, Value> = Map::new();
        task.insert(subject_field_name.clone(), Value::from(subject.clone()));
        task.insert("status".to_string(), Value::from("completed".to_string()));

        let status_field_mapping = get_status_field_mapping();

        let task_field_mappings: Vec<FieldMapping> = vec![status_field_mapping];

        let task = get_todotxt_task_from_map(
            &task_list, &tasklist_field_mappings,
            &task,
            &task_field_mappings,
            &subject_field_name,
        ).unwrap();

        assert!(task.finished)
    }

    #[test]
    fn open_status_should_be_mapped() {
        init_stdout_logging();

        let subject_field_name: String = Faker.fake::<String>();
        let subject: String = Faker.fake::<String>();

        let task_list: Map<String, Value> = Map::new();
        let tasklist_field_mappings: Vec<FieldMapping> = Vec::new();
        let mut task: Map<String, Value> = Map::new();
        task.insert(subject_field_name.clone(), Value::from(subject.clone()));
        task.insert("status".to_string(), Value::from("needsAction".to_string()));

        let status_field_mapping = get_status_field_mapping();

        let task_field_mappings: Vec<FieldMapping> = vec![status_field_mapping];

        let task = get_todotxt_task_from_map(
            &task_list, &tasklist_field_mappings,
            &task,
            &task_field_mappings,
            &subject_field_name,
        ).unwrap();

        assert!(!task.finished)
    }

    #[test]
    fn subject_should_be_extracted() {
        init_stdout_logging();

        let subject_field_name: String = Faker.fake::<String>();
        let subject: String = Faker.fake::<String>();

        let task_list: Map<String, Value> = Map::new();
        let tasklist_field_mappings: Vec<FieldMapping> = Vec::new();
        let mut task: Map<String, Value> = Map::new();
        task.insert(subject_field_name.clone(), Value::from(subject.clone()));

        let task_field_mappings: Vec<FieldMapping> = Vec::new();

        let task = get_todotxt_task_from_map(
            &task_list, &tasklist_field_mappings,
            &task,
            &task_field_mappings,
            &subject_field_name,
        ).unwrap();

        assert_eq!(subject, task.subject)
    }

    #[test]
    fn contexts_from_subject_should_be_mapped() {
        init_stdout_logging();

        let test_data = TestData::new();

        let task_list: Map<String, Value> = Map::new();
        let tasklist_field_mappings: Vec<FieldMapping> = Vec::new();

        let task = get_task_map_with_tags(&test_data);

        let task_field_mappings: Vec<FieldMapping> = Vec::new();

        let task = get_todotxt_task_from_map(
            &task_list, &tasklist_field_mappings,
            &task,
            &task_field_mappings,
            &test_data.subject_field_name,
        ).unwrap();

        assert_eq!(2, task.contexts.len());

        let contexts = task.contexts.clone();

        assert!(contexts.iter().find(|item| *item == &test_data.context1).is_some());
        assert!(contexts.iter().find(|item| *item == &test_data.context2).is_some());
    }

    #[test]
    fn projects_from_subject_should_be_mapped() {
        init_stdout_logging();

        let test_data = TestData::new();

        let task_list: Map<String, Value> = Map::new();
        let tasklist_field_mappings: Vec<FieldMapping> = Vec::new();

        let task = get_task_map_with_tags(&test_data);

        let task_field_mappings: Vec<FieldMapping> = Vec::new();

        let task = get_todotxt_task_from_map(
            &task_list, &tasklist_field_mappings,
            &task,
            &task_field_mappings,
            &test_data.subject_field_name,
        ).unwrap();

        assert_eq!(2, task.projects.len());

        let projects = task.projects.clone();

        assert!(projects.iter().find(|item| *item == &test_data.project1).is_some());
        assert!(projects.iter().find(|item| *item == &test_data.project2).is_some());
    }

    #[test]
    fn tags_from_subject_should_be_mapped() {
        init_stdout_logging();

        let test_data = TestData::new();

        let task_list: Map<String, Value> = Map::new();
        let tasklist_field_mappings: Vec<FieldMapping> = Vec::new();

        let task = get_task_map_with_tags(&test_data);

        let task_field_mappings: Vec<FieldMapping> = Vec::new();

        let task = get_todotxt_task_from_map(
            &task_list, &tasklist_field_mappings,
            &task,
            &task_field_mappings,
            &test_data.subject_field_name,
        ).unwrap();

        assert_tags(&task, &test_data.tag1, &test_data.tag1_value,
                    &test_data.tag2, &test_data.tag2_value);
    }

    #[test]
    fn text_field_mapping_should_be_applied() {
        init_stdout_logging();

        let test_data = TestData::new();

        let task_list: Map<String, Value> = Map::new();
        let tasklist_field_mappings: Vec<FieldMapping> = Vec::new();

        let task = get_task_map_with_tags(&test_data);

        let tag1_mapping = FieldMappingBuilder::new()
            .set_original_field_name(&test_data.tag1)
            .set_target_field_name(&test_data.target_tag1)
            .set_field_type(TaskFieldType::Text)
            .build();

        let tag2_mapping = FieldMappingBuilder::new()
            .set_original_field_name(&test_data.tag2)
            .set_target_field_name(&test_data.target_tag2)
            .set_field_type(TaskFieldType::Text)
            .build();

        let task_field_mappings: Vec<FieldMapping> = vec![tag1_mapping, tag2_mapping];

        let task = get_todotxt_task_from_map(
            &task_list, &tasklist_field_mappings,
            &task,
            &task_field_mappings,
            &test_data.subject_field_name,
        ).unwrap();

        assert_tags(&task, &test_data.target_tag1, &test_data.tag1_value,
                    &test_data.target_tag2, &test_data.tag2_value);
    }

    #[test]
    fn text_field_mapping_skip_if_target_is_blank() {
        init_stdout_logging();

        let test_data = TestData::new();

        let task_list: Map<String, Value> = Map::new();
        let tasklist_field_mappings: Vec<FieldMapping> = Vec::new();

        let task = get_task_map_with_tags(&test_data);

        let tag1_mapping = FieldMappingBuilder::new()
            .set_original_field_name(&test_data.tag1)
            .set_target_field_name("")
            .set_field_type(TaskFieldType::Text)
            .build();

        let tag2_mapping = FieldMappingBuilder::new()
            .set_original_field_name(&test_data.tag2)
            .set_target_field_name("")
            .set_field_type(TaskFieldType::Text)
            .build();

        let task_field_mappings: Vec<FieldMapping> = vec![tag1_mapping, tag2_mapping];

        let task = get_todotxt_task_from_map(
            &task_list, &tasklist_field_mappings,
            &task,
            &task_field_mappings,
            &test_data.subject_field_name,
        ).unwrap();

        assert_eq!(0, task.tags.len());
    }

    #[test]
    fn skip_field_mapping_should_be_applied() {
        init_stdout_logging();

        let test_data = TestData::new();

        let task_list: Map<String, Value> = Map::new();
        let tasklist_field_mappings: Vec<FieldMapping> = Vec::new();

        let task = get_task_map_with_tags(&test_data);

        let tag1_mapping = FieldMappingBuilder::new()
            .set_original_field_name(&test_data.tag1)
            .set_field_type(TaskFieldType::Skip)
            .build();

        let tag2_mapping = FieldMappingBuilder::new()
            .set_original_field_name(&test_data.tag2)
            .set_field_type(TaskFieldType::Skip)
            .build();

        let task_field_mappings: Vec<FieldMapping> = vec![tag1_mapping, tag2_mapping];

        let task = get_todotxt_task_from_map(
            &task_list, &tasklist_field_mappings,
            &task,
            &task_field_mappings,
            &test_data.subject_field_name,
        ).unwrap();

        assert_eq!(0, task.tags.len());
    }

    #[test]
    fn skip_field_if_original_field_name_is_blank() {
        init_stdout_logging();

        let test_data = TestData::new();

        let task_list: Map<String, Value> = Map::new();
        let tasklist_field_mappings: Vec<FieldMapping> = Vec::new();

        let task = get_task_map_with_tags(&test_data);

        let tag1_mapping = FieldMappingBuilder::new()
            .set_original_field_name("")
            .set_target_field_name(&test_data.target_tag1)
            .set_field_type(TaskFieldType::Text)
            .build();

        let tag2_mapping = FieldMappingBuilder::new()
            .set_original_field_name("")
            .set_target_field_name(&test_data.target_tag2)
            .set_field_type(TaskFieldType::Text)
            .build();

        let task_field_mappings: Vec<FieldMapping> = vec![tag1_mapping, tag2_mapping];

        let task = get_todotxt_task_from_map(
            &task_list, &tasklist_field_mappings,
            &task,
            &task_field_mappings,
            &test_data.subject_field_name,
        ).unwrap();

        assert_eq!(2, task.tags.len());

        let tags = task.tags.clone();

        assert!(tags.get(&test_data.target_tag1).is_none());
        assert!(tags.get(&test_data.target_tag2).is_none());
    }

    fn get_status_field_mapping() -> FieldMapping {
        let status_field_name = "status";

        FieldMappingBuilder::new()
            .set_original_field_name(status_field_name)
            .set_target_field_name(status_field_name)
            .set_field_type(TaskFieldType::CompletionStatus)
            .set_open_value("needsAction")
            .set_completed_value("completed")
            .build()
    }
}

#[cfg(test)]
mod date_mapping_tests {
    use chrono::{TimeZone, Utc};
    use fake::{Fake, Faker};
    use serde_json::{Map, Value};

    use crate::{FieldMapping, get_todotxt_task_from_map, TaskFieldType};
    use crate::builder::FieldMappingBuilder;
    use crate::test_utils::{get_task_map_with_tags, init_stdout_logging, TestData};

    #[test]
    fn apply_datetime_mapping() {
        init_stdout_logging();

        let mut test_data = TestData::new();

        let registered_value = Utc.ymd(2022, 09, 08)
            .and_hms(20, 22, 17);

        test_data.tag1_value = registered_value.to_rfc3339();

        let task_list: Map<String, Value> = Map::new();
        let tasklist_field_mappings: Vec<FieldMapping> = Vec::new();

        let task = get_task_map_with_tags(&test_data);

        let tag1_mapping = FieldMappingBuilder::new()
            .set_original_field_name(&test_data.tag1)
            .set_target_field_name(&test_data.target_tag1)
            .set_field_type(TaskFieldType::Date)
            .set_target_datetime_format("%Y-%m-%d")
            .build();

        let task_field_mappings: Vec<FieldMapping> = vec![tag1_mapping];

        let task = get_todotxt_task_from_map(
            &task_list, &tasklist_field_mappings,
            &task,
            &task_field_mappings,
            &test_data.subject_field_name,
        ).unwrap();

        assert_eq!(3, task.tags.len());

        let tags = task.tags.clone();

        assert_eq!("2022-09-08", tags.get(&test_data.target_tag1).unwrap());
    }

    #[test]
    fn return_error_for_unsupported_input_datetime_format() {
        init_stdout_logging();

        let test_data = TestData::new();

        let task_list: Map<String, Value> = Map::new();
        let tasklist_field_mappings: Vec<FieldMapping> = Vec::new();

        let task = get_task_map_with_tags(&test_data);

        let tag1_mapping = FieldMappingBuilder::new()
            .set_original_field_name(&test_data.tag1)
            .set_target_field_name(&test_data.target_tag1)
            .set_field_type(TaskFieldType::Date)
            .set_target_datetime_format("%Y-%m-%d")
            .build();

        let task_field_mappings: Vec<FieldMapping> = vec![tag1_mapping];

        assert!(
            get_todotxt_task_from_map(
                &task_list, &tasklist_field_mappings,
                &task,
                &task_field_mappings,
                &test_data.subject_field_name,
            ).is_err()
        );
    }

    #[test]
    fn target_datetime_format_should_be_presented_as_is() {
        init_stdout_logging();

        let mut test_data = TestData::new();
        let registered_value = Utc.ymd(2022, 09, 08)
            .and_hms(20, 22, 17);
        test_data.tag1_value = registered_value.to_rfc3339();

        let task_list: Map<String, Value> = Map::new();
        let tasklist_field_mappings: Vec<FieldMapping> = Vec::new();

        let task = get_task_map_with_tags(&test_data);

        let target_datetime_format: String = Faker.fake::<String>();

        let tag1_mapping = FieldMappingBuilder::new()
            .set_original_field_name(&test_data.tag1)
            .set_target_field_name(&test_data.target_tag1)
            .set_field_type(TaskFieldType::Date)
            .set_target_datetime_format(&target_datetime_format)
            .build();

        let task_field_mappings: Vec<FieldMapping> = vec![tag1_mapping];

        let task = get_todotxt_task_from_map(
            &task_list, &tasklist_field_mappings,
            &task,
            &task_field_mappings,
            &test_data.subject_field_name,
        ).unwrap();

        assert_eq!(3, task.tags.len());

        let tags = task.tags.clone();

        assert_eq!(&target_datetime_format, tags.get(&test_data.target_tag1).unwrap());
    }
}

#[cfg(test)]
mod error_tests {
    use serde_json::{Map, Value};

    use crate::{FieldMapping, get_todotxt_task_from_map};

    #[test]
    fn return_error_if_subject_field_name_is_missing() {
        let task_list: Map<String, Value> = Map::new();
        let tasklist_field_mappings: Vec<FieldMapping> = Vec::new();
        let task: Map<String, Value> = Map::new();
        let task_field_mappings: Vec<FieldMapping> = Vec::new();

        assert!(
            get_todotxt_task_from_map(
                &task_list, &tasklist_field_mappings,
                &task,
                &task_field_mappings,
                "does-not-exist",
            ).is_err()
        );
    }
}